package by.kolovaitis.trainee.task8

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import by.kolovaitis.trainee.R


class Task8Fragment : Fragment() {

    private val model: Task8ViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task8, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val counter = view.findViewById<TextView>(R.id.tvCounterFragment)
        model.counter.observe(viewLifecycleOwner, Observer<Int>{
            counter.text = it.toString()
        })

    }


}