package by.kolovaitis.trainee.task8


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class Task8ViewModel : ViewModel() {
    companion object {
        const val TIMER_PERIOD: Long = 1000
    }

    private val timer = Timer()
    private val _counter: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>(0)
    }
    val counter: LiveData<Int> get() = _counter
    private val _canStartTimer: MutableLiveData<Boolean> by lazy {
        MutableLiveData(true)
    }
    val canStartTimer: LiveData<Boolean> get() = _canStartTimer

    fun increment() {
        _counter.postValue(_counter.value?.plus(1))
    }

    fun startTimer() {
        val task = object : TimerTask() {
            override fun run() {
                increment()
            }
        }
        timer.schedule(task, TIMER_PERIOD, TIMER_PERIOD)
        _canStartTimer.postValue(false)
    }

    fun stopTimer() {
        timer.cancel()
        _canStartTimer.postValue(true)
    }
}