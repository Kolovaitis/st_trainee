package by.kolovaitis.trainee.task8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import by.kolovaitis.trainee.R
import kotlinx.android.synthetic.main.activity_task8.*

class Task8Activity : AppCompatActivity() {

    val model: Task8ViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task8)
       setupViews()
    }
    private fun setupViews(){
        model.counter.observe(this, Observer<Int> {
            if(it%2==0)
                tvCounterActivity.text = it.toString()
        })
        bIncrementCounter.setOnClickListener {
            model.increment()
        }
        bStartTimer.apply {
            setOnClickListener {
                model.startTimer()
            }
            model.canStartTimer.observe(this@Task8Activity, Observer<Boolean> {
                this.isEnabled = it
            })
        }
    }

    override fun onPause() {
        super.onPause()
        model.stopTimer()
    }
}