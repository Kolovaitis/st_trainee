package by.kolovaitis.trainee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import by.kolovaitis.trainee.task6.Task6Activity
import by.kolovaitis.trainee.task1.MainActivity
import by.kolovaitis.trainee.task11.ui.Task11Activity
import by.kolovaitis.trainee.task2.Task2Activity
import by.kolovaitis.trainee.task3.Task3Activity
import by.kolovaitis.trainee.task4.Task4Activity
import by.kolovaitis.trainee.task5.Task5Activity
import by.kolovaitis.trainee.task8.Task8Activity
import by.kolovaitis.trainee.task9.Task9Activity

class StartActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)


    }
    fun startFirstTask(view: View){
        startTask(MainActivity::class.java)
    }
    fun startSecondTask(view:View){
        startTask(Task2Activity::class.java)
    }
    fun startThirdTask(view: View){
        startTask(Task3Activity::class.java)
    }
    fun startFourthTask(view: View){
        startTask(Task4Activity::class.java)
    }
    fun startFifthTask(view: View){
        startTask(Task5Activity::class.java)
    }
    fun startSixthTask(view: View){
        startTask(Task6Activity::class.java)
    }
    fun startEighthTask(view: View){
        startTask(Task8Activity::class.java)
    }
    fun startNinthTask(view: View){
        startTask(Task9Activity::class.java)
    }
    fun startEleventhTask(view: View){
        startTask(Task11Activity::class.java)
    }


    private fun <T>startTask(activity:Class<T>){
        val intent = Intent(this, activity)
        startActivity(intent)
    }


}