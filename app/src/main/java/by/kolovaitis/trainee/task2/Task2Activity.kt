package by.kolovaitis.trainee.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.kolovaitis.trainee.R

class Task2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task2)
    }
}