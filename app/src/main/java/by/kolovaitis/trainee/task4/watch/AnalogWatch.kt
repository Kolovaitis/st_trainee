package by.kolovaitis.trainee.task4.watch

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.View
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


class AnalogWatch(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    companion object {
        private const val circleColor: Int = Color.WHITE
        private const val hourArrowColor: Int = Color.GREEN
        private const val minuteArrowColor: Int = Color.DKGRAY
        private const val secondArrowColor: Int = Color.BLUE
        private const val tickColor = Color.BLACK
    }

    private val TICK_LENGTH = convertDpToPixel(12f)
    private val TICK_WIDTH = convertDpToPixel(4f)
    private val TEXT_SIZE = convertDpToPixel(24f)
    private val HOUR_WIDTH = convertDpToPixel(8f)
    private val MINUTE_WIDTH = convertDpToPixel(4f)
    private val SECOND_WIDTH = convertDpToPixel(2f)
    private var hourLength = convertDpToPixel(48f)
    private var minuteLength = convertDpToPixel(96f)
    private var secondLength = convertDpToPixel(128f)

    private val circlePaint by lazy {
        Paint().apply {
            color = circleColor
            style = Paint.Style.FILL
        }
    }

    private val hourPaint by lazy {
        Paint().apply {
            color = hourArrowColor
            strokeWidth = HOUR_WIDTH
        }
    }

    private val minutePaint by lazy {
        Paint().apply {
            color = minuteArrowColor
            strokeWidth = MINUTE_WIDTH
        }
    }

    private val secondPaint by lazy {
        Paint().apply {
            color = secondArrowColor
            strokeWidth = SECOND_WIDTH
        }
    }

    private val tickPaint by lazy {
        Paint().apply {
            color = tickColor
            strokeWidth = TICK_WIDTH
            textSize = TEXT_SIZE
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas != null) {
            val centerX = width / 2f
            val centerY = height / 2f

            drawDial(canvas, centerX, centerY)

            drawTime(canvas, centerX, centerY)

        }
        GlobalScope.launch {
            delay(1000)
            invalidate()
        }
    }

    private fun drawTime(canvas: Canvas, centerX: Float, centerY: Float) {
        val calendar = Calendar.getInstance()
        val time = calendar.time
        val hour = time.hours
        val minutes = time.minutes
        val seconds = time.seconds
        drawArrow(canvas, 360 * hour / 12f, centerX, centerY, hourLength, hourPaint)
        drawArrow(canvas, 360 * minutes / 60f, centerX, centerY, minuteLength, minutePaint)
        drawArrow(canvas, 360 * seconds / 60f, centerX, centerY, secondLength, secondPaint)
    }

    private fun drawDial(canvas: Canvas, centerX: Float, centerY: Float) {
        canvas.drawCircle(
            centerX,
            centerY,
            centerX,
            circlePaint
        )
        for (i in 1..12) {
            canvas.drawLine(
                centerX,
                0F,
                centerX,
                TICK_LENGTH,
                tickPaint
            )
            canvas.rotate(360 / 12f, centerX, centerY)
        }

        drawTextCenter(canvas, "12", centerX, TICK_LENGTH + TEXT_SIZE, tickPaint)
        drawTextCenter(canvas, "6", centerX, height - TICK_LENGTH - TEXT_SIZE, tickPaint)
        drawTextCenter(canvas, "3", width - TICK_LENGTH - TEXT_SIZE, centerY, tickPaint)
        drawTextCenter(canvas, "9", TICK_LENGTH + TEXT_SIZE, centerY, tickPaint)

    }

    private fun drawArrow(
        canvas: Canvas,
        angle: Float,
        centerX: Float,
        centerY: Float,
        length: Float,
        paint: Paint
    ) {
        canvas.rotate(angle, centerX, centerY)
        canvas.drawLine(centerX, centerY, centerX, centerY - length, paint)
        canvas.rotate(angle * 11, centerX, centerY)
    }

    private fun drawTextCenter(canvas: Canvas, text: String, x: Float, y: Float, paint: Paint) {
        val bounds = Rect()
        tickPaint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, x - bounds.right / 2, y - bounds.top / 2, paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // Try for a width based on our minimum
        val minw: Int = paddingLeft + paddingRight + suggestedMinimumWidth
        val w: Int = View.resolveSizeAndState(minw, widthMeasureSpec, 1)
        hourLength = w / 4f
        minuteLength = w / 3.5f
        secondLength = w / 3f

        setMeasuredDimension(w, w)
    }

    private fun convertDpToPixel(dp: Float): Float {
        val resources: Resources = context.resources
        val metrics: DisplayMetrics = resources.displayMetrics
        return dp * (metrics.densityDpi / 160f)
    }

}