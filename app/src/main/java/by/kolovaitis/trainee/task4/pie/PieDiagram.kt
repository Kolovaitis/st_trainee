package by.kolovaitis.trainee.task4.pie

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import kotlin.random.Random

class PieDiagram(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    private val colors = mutableListOf<Int>()
    private val randomColor: Int
        get() = Color.argb(
            255,
            Random.nextInt(255),
            Random.nextInt(255),
            Random.nextInt(255)
        )

    private val paint by lazy {
        Paint().apply {
            style = Paint.Style.FILL
            isAntiAlias = true
        }
    }

    private var _numbers = listOf<Int>()

    var numbers: List<Int>
        get() = _numbers
        set(value) {
            _numbers = value
            invalidate()
        }

    private val sectors: List<Float>
        get() {
            val list = mutableListOf<Float>()
            val sum = _numbers.sum()
            _numbers.forEach {
                list.add((360f / sum) * it)
            }
            return list
        }

    private var oval = RectF()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minw: Int = paddingLeft + paddingRight + suggestedMinimumWidth
        val w: Int = View.resolveSizeAndState(minw, widthMeasureSpec, 1)
        oval.set(0f, 0f, w.toFloat(), w.toFloat())
        setMeasuredDimension(w, w)
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas != null) {
            var pastAngle = 0f
            val sectors = this.sectors
            for (i in sectors.indices) {
                drawSector(getColor(i), canvas, pastAngle, sectors[i])
                pastAngle += sectors[i]
            }
        }
    }

    private fun drawSector(color:Int, canvas: Canvas, startAngle:Float, angle:Float){
        paint.color = color
        canvas.drawArc(
            oval,
            startAngle,
            angle,
            true,
            paint
        )
    }

    private fun getColor(i: Int): Int {
        while (colors.size <= i)
            colors.add(randomColor)
        return colors[i]
    }
}