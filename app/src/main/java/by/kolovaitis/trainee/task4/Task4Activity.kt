package by.kolovaitis.trainee.task4

import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task4.pie.PieDiagram
import kotlinx.android.synthetic.main.activity_task4.*

class Task4Activity : AppCompatActivity() {
    lateinit var owlDrawable: AnimationDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task4)
    }
    private fun setupViews(){
        with(ivOwl) {
            setBackgroundResource(R.drawable.owl_animation)
            owlDrawable = background as AnimationDrawable
        }
        pdNumbers.numbers = listOf(5, 3, 10, 2, 7)
    }

    private fun startAnimation() {
        owlDrawable?.start()
    }

    private fun stopAnimation() {
        owlDrawable?.stop()
    }

    override fun onResume() {
        super.onResume()
        startAnimation()
    }

    override fun onPause() {
        super.onPause()
        stopAnimation()
    }

}