package by.kolovaitis.trainee.task3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task3.transformation.CircularTransformation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_task3.*

class Task3Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task3)
    }

    fun loadImage(v: View){
        Picasso.with(this)
            .load(edImageAddress.text.toString())
            .placeholder(R.drawable.loading)
            .error(R.drawable.error)
            .transform(CircularTransformation())
            .into(imageView)
    }
}