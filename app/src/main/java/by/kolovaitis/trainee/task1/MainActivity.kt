package by.kolovaitis.trainee.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import by.kolovaitis.trainee.BuildConfig
import by.kolovaitis.trainee.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
    }
    private fun setupViews(){
        textView.setOnClickListener(this)
        textView2.setOnClickListener {
            exchangeParams()
        }
        tvApiEndpoint.text = BuildConfig.API_ENDPOINT
    }

    private fun exchangeParams() {
        exchangeText(textView, textView2)
        exchangeBackground(textView, textView2)
    }

    private fun exchangeBackground(view1: TextView, view2: TextView) {
        val tempBackground = view1.background
        view1.background = view2.background
        view2.background = tempBackground
    }

    private fun exchangeText(view1: TextView, view2: TextView) {
        val tempText = view1.text
        view1.text = view2.text
        view2.text = tempText
    }

    override fun onClick(v: View?) {
        exchangeParams()
    }

    fun buttonClick(view: View) {
        exchangeParams()
    }
}