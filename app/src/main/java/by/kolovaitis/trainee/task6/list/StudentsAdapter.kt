package by.kolovaitis.trainee.task6.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.trainee.R

class StudentsAdapter(
    private var dataset: List<LiveStudent>,
    private val onClickAction: (view: ImageView, id:Long) -> Unit
) :
    RecyclerView.Adapter<StudentsAdapter.StudentsViewHolder>() {

    class StudentsViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StudentsAdapter.StudentsViewHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.student_preview, parent, false) as CardView

        return StudentsViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: StudentsViewHolder, position: Int) {
        val cardView = holder.cardView
        val image = cardView.findViewById<ImageView>(R.id.ivProfile)
        val name = cardView.findViewById<TextView>(R.id.tvName)
        val surname = cardView.findViewById<TextView>(R.id.tvSurname)
        val setup: (student: Student) -> Unit = {
            image.setImageResource(
                cardView.resources.getIdentifier(
                    it.imageName,
                    "drawable",
                    cardView.context.packageName
                )
            )
            name.text = it.name
            surname.text = it.surname
        }
        setup(dataset[position].student)
        dataset[position].addListener(setup)
        cardView.setOnClickListener {
            onClickAction(image, dataset[position].student.id!!)
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            image.transitionName = "imageTransition"
        }
    }

    override fun getItemCount() = dataset.size

    fun datasetChanged(newDataset: List<LiveStudent>) {
        dataset = newDataset
        this.notifyDataSetChanged()
    }
}