package by.kolovaitis.trainee.task6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task6.list.LiveStudent
import by.kolovaitis.trainee.task6.list.Student
import by.kolovaitis.trainee.task6.list.StudentsManager
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class RedactorFragment : Fragment() {

    var currentStudent: LiveStudent? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_student_redactor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val studentId = arguments?.getLong("id")
        currentStudent = studentId?.let { StudentsManager.getStudentById(it) }
        setupViews(view)
    }
    private fun setupViews(view:View){
        val nameView = view.findViewById<TextView>(R.id.tvName)
        val surnameView = view.findViewById<TextView>(R.id.tvSurname)
        val imageView = view.findViewById<ImageView>(R.id.ivProfileBig)
        val setupView: (student: Student) -> Unit = {
            nameView.text = it.name
            surnameView.text = it.surname
            imageView.setImageResource(
                view.resources.getIdentifier(
                    it.imageName,
                    "drawable",
                    view.context.packageName
                )
            )
        }
        currentStudent?.student?.let { setupView(it) }
        currentStudent?.addListener(setupView)
        view.findViewById<Button>(R.id.bChange).setOnClickListener {
            (activity as Task6Activity).redactStudent(currentStudent)
        }
        view.findViewById<Button>(R.id.bDeletePerson).setOnClickListener {
            deleteStudent()
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.findViewById<FloatingActionButton>(R.id.fab)?.hide()
    }

    override fun onDetach() {
        super.onDetach()
        activity?.findViewById<FloatingActionButton>(R.id.fab)?.show()

    }
    private fun deleteStudent() {
        currentStudent?.let { StudentsManager.removeStudent(it) }
        activity?.onBackPressed()
    }


}