package by.kolovaitis.trainee.task6

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task6.list.StudentsAdapter
import by.kolovaitis.trainee.task6.list.StudentsManager
import com.google.android.material.floatingactionbutton.FloatingActionButton


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ListFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_students_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewManager = LinearLayoutManager(activity)
        viewAdapter =
            StudentsAdapter(StudentsManager.students.value!!) { view, id -> openProfile(view, id) }

        recyclerView = view.findViewById<RecyclerView>(R.id.rvStudents).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        StudentsManager.students.observe(viewLifecycleOwner, Observer { list ->
            (viewAdapter as StudentsAdapter).datasetChanged(list)
        })
    }





    private fun openProfile(image: ImageView, studentId: Long) {
        sharedElementReturnTransition = TransitionInflater.from(activity)
            .inflateTransition(R.transition.change_image_transition)
        exitTransition =
            TransitionInflater.from(activity).inflateTransition(android.R.transition.explode)

        val fragment: Fragment = RedactorFragment()
        fragment.sharedElementEnterTransition = TransitionInflater.from(activity)
            .inflateTransition(R.transition.change_image_transition)
        fragment.enterTransition =
            TransitionInflater.from(activity).inflateTransition(android.R.transition.explode)
        val args = Bundle()
        args.putLong("id", studentId)
        fragment.arguments = args

        val ft: FragmentTransaction = requireFragmentManager().beginTransaction()
            .replace(R.id.fragmentTask6, fragment)
            .addToBackStack("transaction")
            .addSharedElement(image, "imageTransition")
        ft.commit()

    }
}