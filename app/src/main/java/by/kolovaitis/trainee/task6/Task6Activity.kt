package by.kolovaitis.trainee.task6

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.GridLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task6.list.LiveStudent
import by.kolovaitis.trainee.task6.list.Student
import by.kolovaitis.trainee.task6.list.StudentsManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_task6.*


class Task6Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task6)
        StudentsManager.refreshStudentsList(this)
        setupFab()
    }

    private fun setupFab() {
        fab.setOnClickListener {
            redactStudent {
                StudentsManager.addStudent(it)
            }
        }
    }

    private val emptyStudent: LiveStudent
        get() = LiveStudent(
            Student(
                "",
                "",
                resources.getStringArray(R.array.profile_images)[0]
            )
        )

    fun redactStudent(
        startValue: LiveStudent? = null,
        processStudent: ((LiveStudent) -> Unit)? = null
    ) {
        val student = startValue ?: emptyStudent
        setupAlertDialog(student, processStudent).show()
    }

    private fun setupAlertDialog(student: LiveStudent, processStudent: ((LiveStudent) -> Unit)? = null): AlertDialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.layoutInflater
        val view: View = inflater.inflate(R.layout.popup_redactor, null)

        builder.setView(view)

        val images = view.findViewById<GridLayout>(R.id.glProfileImages).children
        selectImage(images, images.find { it.tag.toString() == student.imageName }!!)
        images.forEach { view ->
            view.setOnClickListener {
                selectImage(images, it)
                student.imageName = it.tag as String
            }
        }
        val nameEdit = view.findViewById<EditText>(R.id.etName)
        nameEdit.setText(student.name)
        val surnameEdit = view.findViewById<EditText>(R.id.etSurname)
        surnameEdit.setText(student.surname)
        val button = view.findViewById<Button>(R.id.bOk)
        val dialog = builder.create()
        button.setOnClickListener {
            val newName = nameEdit.text.toString()
            val newSurname = surnameEdit.text.toString()
            if (newName.isNotEmpty() && (newSurname.isNotEmpty())) {
                student.name = newName
                student.surname = newSurname
                dialog.cancel()
                processStudent?.let { it1 -> it1(student) }
            }

        }
        return dialog
    }

    private fun selectImage(images: Sequence<View>, image: View) {
        images.forEach {
            it.alpha = 0.4f
        }
        image.alpha = 1f
    }
}