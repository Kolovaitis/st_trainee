package by.kolovaitis.trainee.task6.list

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.kolovaitis.trainee.R


object StudentsManager {
    private var sqlHelper: SQLiteStudentsHelper? = null


    private val listStudents by lazy<MutableList<LiveStudent>> {
        mutableListOf()
    }
    private val _students by lazy<MutableLiveData<List<LiveStudent>>> {
        MutableLiveData(listStudents)
    }
    val students: LiveData<List<LiveStudent>> get() = _students
    fun refreshStudentsList(context: Context) {
        sqlHelper = SQLiteStudentsHelper(context)
        DBHelper.db = sqlHelper?.writableDatabase
        listStudents.clear()
        DBHelper.studentsList?.forEach { student ->
            listStudents.add(LiveStudent(student).apply { addListener { updateStudent(it) } })
        }
        _students.postValue(listStudents)
    }

    fun addStudent(student: Student) {
        listStudents.add(LiveStudent(student).apply { addListener { updateStudent(it) } })
        _students.postValue(listStudents)
        DBHelper.insertStudent(student)
    }

    fun addStudent(student: LiveStudent) {
        listStudents.add(student.apply { addListener { updateStudent(it) } })
        _students.postValue(listStudents)
        DBHelper.insertStudent(student.student)
    }

    fun removeStudent(student: Student) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            listStudents.removeIf { it.student == student }
        } else {
            listStudents.remove(listStudents.find { it.student == student })
        }
        _students.postValue(listStudents)
        DBHelper.removeStudent(student)
    }

    fun removeStudent(student: LiveStudent) {
        listStudents.remove(student)
        _students.postValue(listStudents)
        DBHelper.removeStudent(student.student)
    }

    private fun updateStudent(student: Student) {
        DBHelper.updateStudent(student)
    }

    fun getStudentById(id: Long): LiveStudent? = listStudents.find { it.student?.id == id }
}

class LiveStudent(val student: Student) {
    private val listeners: MutableList<(student: Student) -> Unit> by lazy {
        mutableListOf<(student: Student) -> Unit>()
    }

    fun addListener(listener: (student: Student) -> Unit) {
        listeners.add(listener)
    }

    fun removeListener(listener: (student: Student) -> Unit) {
        listeners.remove(listener)
    }

    private fun notifyListeners() {
        listeners.forEach { it(student) }
    }

    var name: String
        get() = student.name
        set(value) {
            student.name = value
            notifyListeners()
        }
    var surname: String
        get() = student.surname
        set(value) {
            student.surname = value
            notifyListeners()
        }
    var imageName: String
        get() = student.imageName
        set(value) {
            student.imageName = value
            notifyListeners()
        }

}

data class Student(
    var name: String,
    var surname: String,
    var imageName: String,
    var id: Long? = null
)

class SQLiteStudentsHelper(context: Context) : SQLiteOpenHelper(
    context,
    context.getString(R.string.db_name),
    null,
    context.getString(R.string.db_version).toInt()
) {
    companion object {
        val TABLE_NAME = "students"
        val ID = "id"
        val NAME = "name"
        val SURNAME = "surname"
        val IMAGE_NAME = "image"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "create table $TABLE_NAME ("
                    + "$ID integer primary key autoincrement,"
                    + "$NAME text,"
                    + "$SURNAME text,"
                    + "$IMAGE_NAME text" + ");"
        )
        DBHelper.db = db
        insertTestData()
    }

    fun insertTestData() {
        DBHelper.insertStudent(Student("Иван", "Каратаваев", "profile_1"))
        DBHelper.insertStudent(Student("Игорь", "Савельев", "profile_2"))
        DBHelper.insertStudent(Student("Алеся", "Панова", "profile_4"))
    }


    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME");
        onCreate(db);
    }
}

object DBHelper {
    var db: SQLiteDatabase? = null
    val studentsList: List<Student>
        get() {
            if (db != null) {
                val cursor: Cursor = db!!.query(
                    SQLiteStudentsHelper.TABLE_NAME, null,
                    null, null, null, null, null
                )
                val list = mutableListOf<Student>()
                if (cursor.moveToFirst()) {
                    val idColIndex = cursor.getColumnIndex(SQLiteStudentsHelper.ID)
                    val nameColIndex = cursor.getColumnIndex(SQLiteStudentsHelper.NAME)
                    val surnameColIndex = cursor.getColumnIndex(SQLiteStudentsHelper.SURNAME)
                    val imageNameColIndex =
                        cursor.getColumnIndex(SQLiteStudentsHelper.IMAGE_NAME)
                    do {
                        list.add(
                            Student(
                                cursor.getString(nameColIndex),
                                cursor.getString(surnameColIndex),
                                cursor.getString(imageNameColIndex),
                                cursor.getInt(idColIndex).toLong()
                            )
                        )
                    } while (cursor.moveToNext())
                }

                return list
            }
            return emptyList()
        }

    fun insertStudent(student: Student) {
        student.id = db?.insert(SQLiteStudentsHelper.TABLE_NAME, null, ContentValues().apply {
            put(SQLiteStudentsHelper.NAME, student.name)
            put(SQLiteStudentsHelper.IMAGE_NAME, student.imageName)
            put(SQLiteStudentsHelper.SURNAME, student.surname)
        })
    }

    fun updateStudent(student: Student) {
        db?.update(SQLiteStudentsHelper.TABLE_NAME, ContentValues().apply {
            put(SQLiteStudentsHelper.NAME, student.name)
            put(SQLiteStudentsHelper.IMAGE_NAME, student.imageName)
            put(SQLiteStudentsHelper.SURNAME, student.surname)
        }, "${SQLiteStudentsHelper.ID} = ${student.id}", null)
    }

    fun removeStudent(student: Student) {
        db?.delete(
            SQLiteStudentsHelper.TABLE_NAME,
            "${SQLiteStudentsHelper.ID} = ${student.id}",
            null
        )
    }
}
