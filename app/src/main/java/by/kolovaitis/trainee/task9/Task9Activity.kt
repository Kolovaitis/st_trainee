package by.kolovaitis.trainee.task9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.databinding.ActivityTask9Binding

class Task9Activity : AppCompatActivity() {
    val viewmodel: Task9ViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binder =
            DataBindingUtil.setContentView<ActivityTask9Binding>(this, R.layout.activity_task9)
        binder.viewmodel = viewmodel
    }
}