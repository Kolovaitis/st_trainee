package by.kolovaitis.trainee.task9

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import com.squareup.picasso.Picasso


class Task9ViewModel : ViewModel() {
    companion object {
        const val DEFAULT_NAME = "Василий Андреевич Пупкин"
        const val DEFAULT_AGE = 34
        const val DEFAULT_IS_MALE = true
        const val DEFAULT_IMAGE_ADDRESS =
            "https://rolshtory-minsk.by/wp-content/uploads/2015/08/team_member4.jpg"

    }

    val name: String get() = DEFAULT_NAME
    val age: Int get() = DEFAULT_AGE
    val isMale: Boolean get() = DEFAULT_IS_MALE
val imageAddress:String get() = DEFAULT_IMAGE_ADDRESS

}