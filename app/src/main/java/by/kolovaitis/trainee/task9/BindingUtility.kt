package by.kolovaitis.trainee.task9

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object BindingUtility {
    @BindingAdapter("android:src")
    @JvmStatic
    fun setImageUrl(imageView: ImageView, url: String) {
        Picasso.with(imageView.context)
            .load(url)
            .noFade()
            .into(imageView)
    }
}