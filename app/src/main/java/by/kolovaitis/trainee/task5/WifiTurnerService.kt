package by.kolovaitis.trainee.task5

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.provider.Settings

class WifiTurnerService : Service() {
    private val localBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        fun getService(): WifiTurnerService = this@WifiTurnerService
    }

    private val wifiManager by lazy {
        applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    }

    override fun onBind(intent: Intent): IBinder = localBinder

    fun setWifiState(isOn: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val panelIntent = Intent(Settings.Panel.ACTION_WIFI)
            panelIntent.flags = FLAG_ACTIVITY_NEW_TASK
            startActivity(panelIntent)
        } else {
            wifiManager.isWifiEnabled = isOn
        }
    }

}