package by.kolovaitis.trainee.task5

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class WifiStateReceiver(private val onWifiStateChanged: () -> Unit) : BroadcastReceiver() {


    override fun onReceive(context: Context, intent: Intent) {
        onWifiStateChanged()
    }

}