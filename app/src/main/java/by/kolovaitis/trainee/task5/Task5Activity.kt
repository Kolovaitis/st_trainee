package by.kolovaitis.trainee.task5

import android.content.*
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.IBinder
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task5.WifiTurnerService.LocalBinder
import kotlinx.android.synthetic.main.activity_task5.*


class Task5Activity : AppCompatActivity() {

    private val wifiManager by lazy {
        applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
    }

    private val isWifiEnabled: Boolean get() = wifiManager.isWifiEnabled

    private var _wifiStateView = false

    var wifiStateView: Boolean
        get() = _wifiStateView
        set(value) {
            _wifiStateView = true
            if (value != switchWifi.isChecked)
                switchWifi.isChecked = value
            if (value) {
                tvWifiState.text = applicationContext.resources.getText(R.string.wifi_on)
                tvWifiState.setTextColor(
                    getColorFromResources(R.color.green)
                )
            } else {
                tvWifiState.text = applicationContext.resources.getText(R.string.wifi_off)
                tvWifiState.setTextColor(
                    getColorFromResources(R.color.red)
                )
            }
        }

    private fun getColorFromResources(resId: Int): Int =
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            applicationContext.resources.getColor(resId, theme)
        } else {
            applicationContext.resources.getColor(resId)
        }

    private val wifiStateChangedIntentFilter = IntentFilter("android.net.wifi.WIFI_STATE_CHANGED")

    private val wifiStateReceiver by lazy {
        WifiStateReceiver { wifiStateView = isWifiEnabled }
    }

    private val intentWifiTurnerService by lazy {
        Intent(this, WifiTurnerService::class.java)
    }

    private var turnerService: WifiTurnerService? = null

    private var mBound: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task5)
        setupViews()
    }

    private fun setupViews() {
        switchWifi.setOnCheckedChangeListener { buttonView, isChecked ->
            if (mBound)
                turnerService?.setWifiState(isChecked)
        }

        wifiStateView = isWifiEnabled
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(wifiStateReceiver, wifiStateChangedIntentFilter)
        bindService(intentWifiTurnerService, mConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(wifiStateReceiver)
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(
            className: ComponentName,
            service: IBinder
        ) {
            val binder = service as LocalBinder
            turnerService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }
}