package by.kolovaitis.trainee.task11.modules

import by.kolovaitis.trainee.task11.presenter.Task11ViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    viewModel {
        Task11ViewModel(get())
    }
}