package by.kolovaitis.trainee.task11.network

import by.kolovaitis.trainee.task11.data.Currency

class CurrencyRepository(private val retrofitCurrences: NBRBApi) :
    BaseRepository<CurrencyRepository.Params, CurrencyRepository.Result>() {

    override suspend fun doWork(params: Params): Result {
        var result =
            try {
                retrofitCurrences
                    .getData(params.periodicity)
                    .await()
            } catch (e: Exception) {
                null
            }
        return Result(result?.body())
    }

    data class Params(val periodicity: Int)
    data class Result(val currencies: List<Currency>?)
}