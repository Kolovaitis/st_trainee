package by.kolovaitis.trainee.task11.modules

import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task11.network.CurrencyRepository
import by.kolovaitis.trainee.task11.network.NBRBApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val retrofitModule = module {

    single {
        retrofit(androidApplication().getString(R.string.base_url))
    }
    single {
        get<Retrofit>().create(NBRBApi::class.java)
    }
    single{
        CurrencyRepository(get())
    }
}

private fun retrofit(baseUrl: String) = Retrofit.Builder()
    .baseUrl(baseUrl)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .build()