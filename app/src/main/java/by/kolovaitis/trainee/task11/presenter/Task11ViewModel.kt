package by.kolovaitis.trainee.task11.presenter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.kolovaitis.trainee.task11.data.Currency
import by.kolovaitis.trainee.task11.network.CurrencyRepository
import kotlinx.coroutines.*


class Task11ViewModel(val currencyRepository: CurrencyRepository) : ViewModel() {

    private val _currencies: MutableLiveData<List<Currency>> by lazy {
        MutableLiveData<List<Currency>>()
    }
    val currencies: LiveData<List<Currency>> get() = _currencies

    private val _isLoading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    val isLoading: LiveData<Boolean> get() = _isLoading

    init {
        refreshCurrencies()
    }

    private fun refreshCurrencies(periodicity: Int = 0) {
        _isLoading.postValue(true)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = currencyRepository.doWork(CurrencyRepository.Params(periodicity))
                _currencies.postValue(result.currencies ?: listOf())
                _isLoading.postValue(false)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}