package by.kolovaitis.trainee.task11.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task11.data.Currency

class CurrencyAdapter(private var dataset: List<Currency> = listOf()) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    class ViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.currency_item, parent, false) as CardView
        return ViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(dataset[position]) {
            holder.cardView.findViewById<TextView>(R.id.tvAbbreviation).text = Cur_Abbreviation
            holder.cardView.findViewById<TextView>(R.id.tvCurrency).text =
                "$Cur_Scale $Cur_Name = $Cur_OfficialRate б.р."
        }


    }

    override fun getItemCount() = dataset.size
    fun refreshDataSet(newDataset:List<Currency>){
        dataset = newDataset
        this.notifyDataSetChanged()
    }
}
