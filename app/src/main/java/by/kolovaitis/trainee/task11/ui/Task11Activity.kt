package by.kolovaitis.trainee.task11.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.trainee.R
import by.kolovaitis.trainee.task11.presenter.Task11ViewModel
import kotlinx.android.synthetic.main.activity_task11.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class Task11Activity : AppCompatActivity() {
    private val viewAdapter: CurrencyAdapter by lazy{
        CurrencyAdapter()
    }
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val model: Task11ViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task11)
        setupRecyclerView()
        setupProgressBar()
    }

    private fun setupRecyclerView(){
        viewManager = LinearLayoutManager(this)
        rvCurrency.apply {
            layoutManager = viewManager
            adapter = viewAdapter
            model.currencies.observe(this@Task11Activity, Observer {
                viewAdapter.refreshDataSet(it)
            })
        }
    }

    private fun setupProgressBar(){
        progressBar.apply {
            model.isLoading.observe(this@Task11Activity, Observer {
                this.isVisible = it
            })
        }
    }


}
