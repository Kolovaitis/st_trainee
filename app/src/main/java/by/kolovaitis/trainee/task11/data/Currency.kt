package by.kolovaitis.trainee.task11.data

data class Currency(val Cur_ID:Int, val Date:String, val Cur_Abbreviation:String, val Cur_Scale:Int, val Cur_Name:String, val Cur_OfficialRate:Float)