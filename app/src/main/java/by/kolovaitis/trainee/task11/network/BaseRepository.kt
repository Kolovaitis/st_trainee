package by.kolovaitis.trainee.task11.network

abstract class BaseRepository<Params, Result> {

    abstract suspend fun doWork(params: Params): Result

}